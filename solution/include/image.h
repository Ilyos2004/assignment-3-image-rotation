#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct __attribute__((packed)) image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(size_t width, size_t height);

void destroy_image(struct image* img);
