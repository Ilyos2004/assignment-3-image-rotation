#include "rotate.h"

static void transpose(struct image* img) {
    for (uint64_t row = 0; row < img->height; row++) {
        for (uint64_t col = 0; col < img->width / 2; col++) {
            struct pixel left_pixel = img->data[row * img->width + col];
            struct pixel right_pixel = img->data[row * img->width + (img->width - col - 1)];
            img->data[row * img->width + col] = right_pixel;
            img->data[row * img->width + (img->width - col - 1)] = left_pixel;
        }
    }
}

static void transform_image(struct image* img) {
    struct pixel* new_data = malloc(img->width * img->height * sizeof(struct pixel));
    if (!new_data) {
        fprintf(stderr, "Failed to allocate memory for transformed image\n");
        return;
    }

    for (uint64_t row = 0; row < img->height; row++) {
        for (uint64_t col = 0; col < img->width; col++) {
            new_data[col * img->height + row] = img->data[row * img->width + col];
        }
    }

    destroy_image(img);
    img->data = new_data;
    uint64_t temp = img->width;
    img->width = img->height;
    img->height = temp;
}

enum rotation_result rotate(struct image* img, int64_t angle) {
    angle = (360 - angle) % 360 / 90;
    for (int64_t i = 0; i < angle; i++) {
        transform_image(img);
        transpose(img);
    }

    return ROTATION_OK;
}
