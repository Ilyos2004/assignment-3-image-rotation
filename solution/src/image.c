#include "image.h"

struct image create_image(uint64_t w, uint64_t h) {

    struct image img = {0};

    img.data =calloc(w * h, sizeof(*img.data));
    if (img.data == NULL) {
        return img;
    }

    img.width = w;
    img.height = h;

    return img;
}

void destroy_image(struct image* img) {
    if (img && img->data) {
        free(img->data);
        img->data = NULL;
    }
}
