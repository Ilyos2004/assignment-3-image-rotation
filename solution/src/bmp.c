#include "bmp_headers.h"

static uint64_t calculate_padding(uint64_t width) {
    uint64_t rowSize = width * sizeof(struct pixel);
    uint64_t padding = (4 - rowSize % 4) % 4;
    return padding;
}


enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_BITS_NUMBER;
    }

    if (header.bfType != BMP_SIGNATURE) {
        destroy_image(img);
        return READ_INVALID_SIGNATURE;
    }

    if (header.biSize != HEADER_SIZE) {
        destroy_image(img);
        return READ_INVALID_HEADER;
    }

    if (header.biBitCount != HEADER_BIT_COUNT) {
        destroy_image(img);
        return READ_INVALID_HEADER;
    }

    *img = create_image(header.biWidth, header.biHeight);

    size_t skip = fseek(in, (long) header.bOffBits, SEEK_SET);

    if (skip) {
        destroy_image(img);
        return READ_INVALID_BITS_NUMBER;
    }


    size_t padding = calculate_padding(img->width);

    for (size_t i = 0; i < img->height; i++) {

        size_t read = fread(img->data + i * img->width, sizeof(struct pixel), img->width, in);

        if (read != img->width) {
            destroy_image(img);
            return READ_INVALID_BITS_NUMBER;
        }

        int shift = fseek(in, (long) padding, SEEK_CUR);

        if (shift) {
            destroy_image(img);
            return READ_INVALID_BITS_NUMBER;
        }
    }

    return READ_OK;
}


enum write_status to_bmp(FILE* outputFile, struct image const* img) {

    size_t padding = calculate_padding(img->width);
    size_t pixel_data_size = img->width * img->height * sizeof(struct pixel);
    size_t total_img_size = sizeof(struct bmp_header) + pixel_data_size;
    struct bmp_header header = {
            .bfType = BMP_SIGNATURE,
            .bfileSize = total_img_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = HEADER_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = HEADER_BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = 0,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    size_t write_header = fwrite(&header, sizeof(struct bmp_header), 1, outputFile);

    if (write_header != 1) {
        return WRITE_ERROR;
    }

    uint32_t padding_space = 0;

    for (size_t i = 0; i < img->height; i++) {

        size_t write_pixels = fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, outputFile);
        size_t padding_written = fwrite(&padding_space, 1, padding, outputFile);

        if (write_pixels != img->width || padding_written != padding) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
