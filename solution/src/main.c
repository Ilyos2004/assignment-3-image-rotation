#include "bmp.h"
#include "rotate.h"
#include <stdio.h>

int main(int argc, char** argv) {

    if (argc < 4) {
        fprintf(stderr, "Usage: <input_image> <output_image> <angle>\n");
        return EXIT_FAILURE;;
    }

    char *endptr = NULL;
    int64_t angle = strtoll(argv[3], &endptr, 10);

    FILE *input_image = fopen(argv[1], "rb");

    if (!input_image) {
        fprintf(stderr, "Error opening file '%s'\n", argv[1]);
        return EXIT_FAILURE;;
    }

    struct image img = {0};
    enum read_status read = from_bmp(input_image, &img);

    switch (read) {
        case MEMORY_ALLOCATION_ERROR:
            fprintf(stderr, "Memory error\n");
            break;
        case READ_INVALID_SIGNATURE:
            fprintf(stderr, "Invalid BMP signature\n");
            break;
        case READ_INVALID_BITS_NUMBER:
            fprintf(stderr, "Invalid bits\n");
            break;
        case READ_INVALID_HEADER:
            fprintf(stderr, "Invalid header\n");
            break;
        default:
            fprintf(stderr, "Unknown error\n");
            break;
    }

    fclose(input_image);


    rotate(&img, angle);

    char* output_image_path = argv[2];
    FILE* output_image = fopen(output_image_path, "wb");
    if (!output_image) {
        fprintf(stderr, "Error opening file '%s' for writing\n", output_image_path);
        destroy_image(&img);
        return EXIT_FAILURE;
    }

    enum write_status write = to_bmp(output_image, &img);
    fclose(output_image);
    destroy_image(&img);

    if (write != WRITE_OK) {
        fprintf(stderr, "An error occurred during writing\n");
        return EXIT_FAILURE;
    }
    printf("Done!\n");

    return 0;

}
